<?php


namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;

class ProductController extends Controller
{
    function get()
    {
        $allProducts = Product::all();

        return response()->json(
            $allProducts
        );
    }

    function getById($id)
    {
        $productById = Product::where('id', $id)->get();

        return response()->json([
            'message'   => 'Success',
            'data'      => $productById
        ]);
    }

    public function store(StoreProductRequest $request)
    {
        $productStoreValidation = $request->validated();
        try {
            $imageName = $request->file('image')->hashName();
            $path = $request->file('image')->storeAs('uploads/img', $imageName);
            $productStoreValidation['image'] = $path;

            $addProduct = Product::create($productStoreValidation);
            return response()->json(
                $addProduct
            );
        } catch (\Exception $e) {
            return response()->json([
                'message'   => 'Err',
                'error'     => $e->getMessage()
            ]);
        }
    }

    // function post(Request $request)
    // {
    //     $image = $request->file('image');
    //     $image->storeAs('public/img', $image->hashName());

    //     $product = new Product;
    //     $product->name          = $request->name;
    //     $product->price         = $request->price;
    //     $product->quantity      = $request->quantity;
    //     $product->active        = $request->active;
    //     $product->description   = $request->description;
    //     $product->image         = $image->hashName();

    //     $product->save();

    //     return response()->json([
    //         'message'   => 'Success',
    //         'data'      => $product
    //     ]);
    // }

    public function update(UpdateProductRequest $request, $id)
    {
        $productUpdateValidation = $request->validated();
        $productById = Product::find($id);

        try {
            if ($request->file('image')) {
                $imageName = $request->file('image')->hashName();
                $path = $request->file('image')->storeAs('uploads/img', $imageName);

                $imagePath = public_path() . '/' . $productById->image;
                unlink($imagePath);

                $productUpdateValidation['image'] = $path;
            }

            $productById->update($productUpdateValidation);

            return response()->json([
                'success' => true,
                'message' => 'success',
                'data'    => $productById
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Err',
                'error' => $e->getMessage()
            ]);
        }
    }

    //function put($id, Request $request)
    // function put($id, Request $request)
    // {
    //     //$product = Product::where('id', $id)->first();
    //     $product = Product::findOrFail($id);
    //     if ($request->has('image')) {
    //         Storage::disk('local')->delete('public/img/' . $product->image);
    //         $image = $request->file('image');
    //         $image->save('public/img', $image->hashName());

    //         $product->update([
    //             'image' => $image->hashName(),
    //             'name'  => $request->name ? $request->name : $product->name,
    //             'price' => $request->price ? $request->price : $product->price,
    //             'quantity' => $request->quantity ? $request->quantity : $product->quantity,
    //             'active' => $request->active ? $request->active : $product->active,
    //             'description'   => $request->description ? $request->description : $product->description

    //         ]);
    //         return response()->json(
    //             [
    //                 'message' => "PUT Method Success" . $product
    //             ]
    //         );
    //     }
    //     return response()->json(
    //         [
    //             'message' => 'Product with id ' . $product->id . ' not found'
    //         ],
    //         400
    //     );
    // }

    public function destroy($id)
    {
        try {
            $productById = Product::find($id);
            $imagePath = public_path() . '/' . $productById->image;
            unlink($imagePath);
            $productById->delete();
            return response()->json([
                'succes' => true,
                'message' => 'Success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Err',
                'errors' => $e->getMessage()
            ]);
        }
    }

    // function delete($id)
    // {
    //     $product = Product::where('id', $id)->first();
    //     if ($product) {
    //         $product->delete();
    //         return response()->json(
    //             [
    //                 'message' => 'DELETE Product id ' . $id . ' Success'
    //             ]
    //         );
    //     }
    //     return response()->json(
    //         [
    //             'message' => 'Product with id ' . $id . ' not found'
    //         ]
    //     );
    // }
}
